package finki.wp.turizam.web;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import finki.wp.turizam.model.jpa.Tmesto;
import finki.wp.turizam.model.jpa.TmestoPicture;
import finki.wp.turizam.service.QueryService;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by Aleksandar on 02.02.2018.
 */
@RestController
public class ResttController {

    @Autowired
    QueryService queryService;

    @RequestMapping("/mesta")
    public Iterable<Tmesto> greeting() {
        return  queryService.findsite();
    }


    @RequestMapping("/mesta/{id}")
    public Tmesto mesto(@PathVariable Long id) {
        return  queryService.getById(id);
    }


    @RequestMapping(value = {"/mesto/{id}/picture"}, method = RequestMethod.GET)
    @ResponseBody
    public void indexx(@PathVariable Long id, HttpServletResponse response) throws IOException, SQLException {
        OutputStream out = response.getOutputStream();

        TmestoPicture bookPicture = queryService.getByMestoId(id);

        String contentDisposition = String.format("inline;filename=\"%s\"",
                bookPicture.picture.fileName + ".png?productId=" + id);

        response.setHeader("Content-Disposition", contentDisposition);
        response.setContentType(bookPicture.picture.contentType);
        response.setContentLength(bookPicture.picture.size);

        IOUtils.copy(bookPicture.picture.data.getBinaryStream(), out);

        out.flush();
        out.close();
    }


    @RequestMapping(value = {"/categor/{id}"}, method = RequestMethod.GET)
    public Iterable<Tmesto> categoryProduct(
            @PathVariable Long id,
            Model model
    ) {
        // ne raboti
        return   queryService.getMestaInCategory(id,10,10);

    }

    @RequestMapping(value = {"/searchrest"}, method = RequestMethod.GET)
    public Iterable<Tmesto> searchrest( @RequestParam String query,Model model ) {

        return queryService.searchMesto(query);
    }

}