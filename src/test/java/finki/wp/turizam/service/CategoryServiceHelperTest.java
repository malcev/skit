package finki.wp.turizam.service;

import finki.wp.turizam.model.jpa.Category;
import finki.wp.turizam.persistence.CategoryRepository;
import finki.wp.turizam.service.impl.CategoryHelperImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class CategoryServiceHelperTest {

    CategoryServiceHelper categoryServiceHelper;

    @Mock
    CategoryRepository categoryRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        categoryServiceHelper = new CategoryHelperImpl(categoryRepository);
    }

    @Test
    public void getTopLevelCategories() {

        List<Category> categories = new ArrayList<>();

        when(categoryRepository.findByParentIsNull()).thenReturn(categories);

        List<Category> categoryList = categoryServiceHelper.getTopLevelCategories();

        assertEquals(categories.size(),categoryList.size());
    }

    @Test
    public void getSubCategories() {
    }

    @Test
    public void createTopLevelCategory() {
    }

    @Test
    public void createCategory() {

        Category category = new Category();
        category.id=1L;
        category.name="cat1";
        category.parent=categoryRepository.findOne(1L);

        when(categoryRepository.save(category)).thenReturn(category);
        when(categoryRepository.findOne(1L)).thenReturn(category);

        Category category1 = categoryServiceHelper.createCategory("cat1",1L);

        assertEquals(category,category1);
    }

    @Test
    public void updateCategoryName() {
    }

    @Test
    public void removeCategory() {
    }
}