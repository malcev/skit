package finki.wp.turizam.service;

import finki.wp.turizam.model.jpa.Category;
import finki.wp.turizam.persistence.CategoryRepository;
import finki.wp.turizam.persistence.QueryRepository;
import finki.wp.turizam.persistence.TmestoPictureRepository;
import finki.wp.turizam.service.impl.QueryServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class QueryServiceTest {

    QueryService queryService;

    @Mock
    QueryRepository queryRepository;

    @Mock
    TmestoPictureRepository tmestoPictureRepository;

    @Mock
    CategoryRepository categoryRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        queryService= new QueryServiceImpl(queryRepository,categoryRepository,tmestoPictureRepository);
    }

    @Test
    public void getMestaInCategory() {

    }

    @Test
    public void getPromotedBooks() {
    }

    @Test
    public void findTopLevelCategories() {
    }

    @Test
    public void findCategories() {

        List<Category> categories = new ArrayList<>();

        when(categoryRepository.findByNameNotNull()).thenReturn(categories);

        List<Category> categoryList = queryService.findCategories();

        assertEquals(categories.size(),categoryList.size());
    }

    @Test
    public void getByMestoId() {
    }

    @Test
    public void searchMesto() {
    }

    @Test
    public void findsite() {
    }

    @Test
    public void getById() {
    }
}