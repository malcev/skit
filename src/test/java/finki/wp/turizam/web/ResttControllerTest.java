package finki.wp.turizam.web;

import finki.wp.turizam.model.jpa.Category;
import finki.wp.turizam.model.jpa.Tmesto;
import finki.wp.turizam.service.QueryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ResttControllerTest {

    MockMvc mockMvc;

    @InjectMocks
    ResttController resttController;

    @Mock
    QueryService queryService;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);

        mockMvc = MockMvcBuilders.standaloneSetup(resttController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    public void greeting() throws Exception{

        Tmesto tmesto1 = new Tmesto();
        tmesto1.id=1L;
        tmesto1.name="Tmesto1";
        tmesto1.description="desc1";
        tmesto1.category=new Category();

        Tmesto tmesto2 = new Tmesto();
        tmesto2.id=1L;
        tmesto2.name="Tmesto1";
        tmesto2.description="desc1";
        tmesto2.category=new Category();

        Tmesto tmesto3 = new Tmesto();
        tmesto3.id=1L;
        tmesto3.name="Tmesto1";
        tmesto3.description="desc1";
        tmesto3.category=new Category();

        List<Tmesto> list = new ArrayList<>();
        list.add(tmesto1);
        list.add(tmesto2);
        list.add(tmesto3);

        when(queryService.findsite()).thenReturn(list);

        mockMvc.perform(get("/mesta")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[1].id").value(1))
                .andExpect(jsonPath("$[2].id").value(1));
    }

    @Test
    public void mesto() {
    }

    @Test
    public void indexx() {
    }

    @Test
    public void categoryProduct() {
    }

    @Test
    public void searchrest() {
    }
}