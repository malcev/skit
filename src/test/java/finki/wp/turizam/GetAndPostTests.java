package finki.wp.turizam;

import static org.apache.http.HttpStatus.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static  io.restassured.RestAssured.*;

import finki.wp.turizam.model.jpa.Tmesto;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.junit.BeforeClass;
import org.junit.Test;

public class GetAndPostTests {

    private RequestSpecification specification;
    private Tmesto tmesto= new Tmesto();


//    @BeforeClass
//    public void inItSpec() {
//        specification = new RequestSpecBuilder()
//                .setContentType(ContentType.JSON)
//                .setBaseUri("http://localhost:8443")
//                .addFilter(new RequestLoggingFilter())
//                .addFilter(new ResponseLoggingFilter())
//                .build();
//    }



    //Get pozitivno scenario

    @Test
    public void getMovie(){

        RestAssured.baseURI="http://localhost:8080";
        given()
                .when()
                .get("/mesta")
                .then().log().all()
                .statusCode(SC_OK)
                .and()
                .body("Title",equalTo("Guardians of the Galaxy Vol. 2"));

    }

    //Get negativno scenario

    @Test
    public void getMovieError(){

        RestAssured.baseURI="http://www.omdbapi.com";
        given()
                .when()
                .get("/?t=Guardians+of+the+Galaxy+Vol.+2")
                .then().log().all()
                .statusCode(SC_UNAUTHORIZED)
                .and()
                .body("Error",equalTo("No API key provided."));

    }


    // Post pozitivno scenario

    @Test
    public void postMovie() {
        given()
                .params("title", "Movie")
                .when()
                .post("http://www.omdbapi.com/?apikey=d9b278c3/movie")
                .then().log().all()
                .statusCode(SC_CREATED);
    }


    // Post negativno scenario

    @Test
    public void movieCanNotBeCreatedWithoutAuth() {
        given()
                .params("title", "Movie")
                .when()
                .post("http://www.omdbapi.com")
                .then().log().all()
                .statusCode(SC_UNAUTHORIZED)
                .and()
                .body("Error",equalTo("No API key provided."));
    }

    
    // Post pozitivno scenario so Java objekt

    @Test
    public void postMoviePOJO(){

        given()
                .specification(specification)
                .body(tmesto
                        //.setDescription("2018")
                        //.setCategory("Malchev")
                        )
                .when()
                .post()
                .then()
                .statusCode(SC_CREATED);
    }


}


